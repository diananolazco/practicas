package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("findAll");

       return this.productRepository.findAll();

    }
    public ProductModel add(ProductModel product){
        System.out.println("add en Product");

       return this.productRepository.save(product);
    }
    public Optional<ProductModel>findById(String id){
        System.out.println("Id de product");

        return this.productRepository.findById(id);
    }

    public ProductModel update(ProductModel products){
        return this.productRepository.save(products);

    }

    public Boolean delete(String id){
        System.out.println("delete product");

        boolean result = false;
    if (this.findById(id).isPresent() == true){

        this.productRepository.deleteById(id);
        result = true;
    }
    return  result;

    }
}
