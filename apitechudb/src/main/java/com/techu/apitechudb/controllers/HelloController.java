package com.techu.apitechudb.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @RequestMapping()
    public String index(){

        return "Hola V2 con mongo";
    }

    @RequestMapping("/hello")
    public String hello(
            @RequestParam(value = "name" , defaultValue = "Tech U Mongo") String name
    ){
        return String.format("Hola %s!", name);
    }
}
