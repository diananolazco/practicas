package com.techucurso.apitechu;

import com.techucurso.apitechu.models.ProductModel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class ApitechuApplication {

	public static ArrayList<ProductModel> productModels;

	public static void main(String[] args) {

		SpringApplication.run(ApitechuApplication.class, args);
	ApitechuApplication.productModels =ApitechuApplication.getTestDate();


	}

	private static ArrayList<ProductModel> getTestDate(){
		ArrayList<ProductModel> productModels = new ArrayList<>();

		productModels.add(
				new ProductModel(
						"1"
						,"PAPAS"
						,20

				)
		);
		productModels.add(
				new ProductModel(
						"2"
						,"REFRESCO"
						,20

				)
		);
		productModels.add(
				new ProductModel(
						"3"
						,"Producto3"
						,20

				)
		);
		return productModels;
	}
}
