package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public  ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

        return new ResponseEntity<>(
                this.productService.findAll(), HttpStatus.OK
     );

    }
    @GetMapping("/products/{id}")
  //  public  ResponseEntity<ProductModel> getProductId(@PathVariable String id ){
    public  ResponseEntity<Object> getProductId(@PathVariable String id ){

        System.out.println("getProductId");
        System.out.println("ID encontrado" + id);
        Optional<ProductModel> result = this.productService.findById(id);
        //result.


        return  new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND

      //          new ProductModel(),HttpStatus.OK
        );

    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct (@RequestBody ProductModel product){

        System.out.println("addProducts");
        System.out.println("La id del producto se va a crear " + product.getId());
        System.out.println("La descripcion se agrega " + product.getDesc());
        System.out.println("El precio se agrega " + product.getPrice());

        return new ResponseEntity<>(
                this.productService.add(product)
                ,HttpStatus.CREATED

        );
    }
    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProducts(@RequestBody ProductModel products, @PathVariable String id){
        System.out.println("updateProducts");
        System.out.println("" +id);
        System.out.println("" + products.getId());
        System.out.println("" +products.getDesc());
        System.out.println(""+ products.getPrice());

        Optional<ProductModel> productUpdate =this.productService.findById(id);

        if(productUpdate.isPresent()){
            System.out.println("Producto para actualizar ");
            this.productService.update(products);
        }
        return  new ResponseEntity<>(
                products, productUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
                //HttpStatus.OK

        );
    }
    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){

        System.out.println("deleteProduct");

       boolean deleteProduct = this.productService.delete(id);

        return  new ResponseEntity<>(
                deleteProduct ? "producto borrado" : "producto no barrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND

        );

    }
}
