package com.techucurso.apitechu.controllers;


import com.techucurso.apitechu.ApitechuApplication;
import com.techucurso.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    static final String  APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;

    }
    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("id es:" + id);

        ProductModel result =
                new ProductModel();

        for(ProductModel product : ApitechuApplication.productModels){
            if(product.getId().equals(id)){
                result = product;
            }
        }

        return result;


    }
    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id nueva es :"+ newProduct.getId() );
        System.out.println("La id nueva es :"+ newProduct.getDesc() );
        System.out.println("La id nueva es :"+ newProduct.getPrice() );

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;

    }
    @PutMapping(APIBaseUrl + "/products/{id}")
    public  ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id)
    {

        System.out.println("updateProduct");
        System.out.println("" + id );
        System.out.println("Actualiza id " + product.getId() );
        System.out.println("actuaiza Desc" + product.getDesc() );
        System.out.println("actualiza Price " + product.getPrice() );


        for(ProductModel productList : ApitechuApplication.productModels){
            if(productList.getId().equals(id)){
            productList.setId(product.getId());
            productList.setDesc(product.getDesc());
            productList.setPrice(product.getPrice());

            }
        }

        return product;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("El id del producto a tomar" + id)

        ;
        ProductModel  result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList: ApitechuApplication.productModels){
            if(productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if(foundCompany == true){
            System.out.println("borrado");
            ApitechuApplication.productModels.remove(result);
        }


        return result;
    }
     @PatchMapping(APIBaseUrl + "/products/{id}")
    public  ProductModel  UpdateProductPar(@PathVariable  String id ,@RequestBody ProductModel produc1){
         System.out.println("updateProductPar");
         System.out.println("id" + id );
     //    System.out.println("desc" + desc);
     //    System.out.println("price" + price);
     //    System.out.println("id" + produc1.getId());
         System.out.println("actuaiza Desc" + produc1.getDesc());
         System.out.println("actualiza Price " + produc1.getPrice() );

         ProductModel result = new ProductModel();

        for(ProductModel productPara: ApitechuApplication.productModels){
             if(productPara.getId().equals(id)){
                result = productPara;

            }
       }

         return result;

    }
}
